Dreamsicle Public License, version 1
---

Copyright 2017, The Lightrail Authors

Definitions:
Base project - The original product, project, or artifact.
Project - The product, project, or artifact that this license
text governs and is referring to.
Common files - Files, code, or resources shared by the base 
project and any derivatives.
Redistributions - Derivatives of the base project.

Anyone is free to copy, modify, publish, use, compile, or
distribute this project, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

Any redistributions must include or link to the source code of 
common files, without any requirements outside having an internet 
connection to download the source code of the common files, and stay 
under this license & include this text.

Redistributions must include or link to a publicly-available log of 
any changes made to the project, as well as credit the creators 
of the base project.

The project or any hardware running the project may not use
any measures to circumvent a redistribution or modified version
of the project being used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.