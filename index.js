const fs = require("fs")
const confJson = JSON.parse(fs.readFileSync("./conf.json"))
const Eris = require("eris")
global.NodeCache = require("node-cache")
global.cache = new NodeCache()

console.log("Initializing Global Variables..")
// Branding stuff
global.brandOwner =  confJson.brandOwner
global.brandName =  confJson.brandName

// Misc.
global.modRoleID =  confJson.modRoleID

// Colors
global.brandColor =  parseInt(confJson.brandColor, 16)
global.infoColor =  parseInt(confJson.infoColor, 16)
global.successColor =  parseInt(confJson.successColor, 16)
global.warningColor =  parseInt(confJson.warningColor, 16)
global.errorColor =  parseInt(confJson.errorColor, 16)

console.log("Creating Client..")
global.bot = new Eris(confJson.token)
bot.on("ready", () => {
    console.log(brandOwner + " " + brandName + " Ready.")
})

console.log("Hashing Directories..")
global.commands = require('./commands/index.js')
global.internals = require('./internals/index.js')

console.log("Populating Cache..")
internals.resolver.init

console.log("Creating messageCreate Handler..")

bot.on("messageCreate", (msg) => {
    if(msg.content.substr(0, confJson.prefix.length) === confJson.prefix) {
        msg.content = msg.content.substr(confJson.prefix.length, msg.content.length)
        
        if (msg.content.includes(' ')) {
            var command = msg.content.substr(0, msg.content.indexOf(' '))
            var argument = msg.content.substr(msg.content.indexOf(' ') + 1)
        } else {
            var command = msg.content
        }
        
        if (internals.resolver.checkIfExists(command)) {
            var tempRand = Math.random()*1011|0 * 100 / Math.floor(Math.random()*10)
            cache.set(tempRand, msg, 15)
            internals.messaging.runCommand(command, tempRand, argument)
        }
    }
})

console.log("Client Connecting..")
bot.connect()