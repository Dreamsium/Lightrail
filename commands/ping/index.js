module.exports = function(tempRand, argument) {
    var msg = cache.get(tempRand)
    bot.createMessage(msg.channel.id,
        {embed: {
            "title": "Response Time",
            "description": "One sec..",
            "color": warningColor
          }
        }
    ).then(newMsg =>
              newMsg.edit(
                {embed: {
                  "title": "Response Time",
                  "description": newMsg.timestamp - msg.timestamp + "ms",
                  "color": infoColor
                  }
                }
              )
          )
    return
}