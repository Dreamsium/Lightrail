module.exports = function(command, tempRand, argument) {
    if (argument === undefined || argument === null) {
        argument = "none"
    }
    var commandPath = require('app-root-path') + "/commands/" + command + '/index.js'
    require(commandPath)(tempRand, argument)
}