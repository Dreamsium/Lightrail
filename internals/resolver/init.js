const Filehound = require('filehound')
const subdirectories = Filehound.create().path(require('app-root-path') + "/commands").directory().findSync()
var trail = require('app-root-path') + "/commands/"

for (var i = 0; i < subdirectories.length; i++) {
    subdirectories[i] = subdirectories[i].substr(trail.length, subdirectories[i].length)
}

cache.set("validCommands", subdirectories)